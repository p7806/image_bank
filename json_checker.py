import os
import json


def discover_files(path):
    file_list = []

    for root, dirs, files in os.walk(path):
        for filename in files:
            if filename.endswith('json'):
                file_list.append(path + filename)
        break
    return file_list


def load_data(fname):
    print(fname)
    with open(fname, 'r') as fp:
        return json.load(fp)


def load_jsons(path):
    flist = discover_files(path)
    stuff_dict = dict()
    for fs in flist:
        fname = fs.split('\\')
        fname = fname[-1].replace('.json', '')
        stuff_dict[fname] = load_data(fs)
    return stuff_dict


def order_dict(xdict):
    if isinstance(xdict, dict):
        for element in xdict:
            deduplicated = list(dict.fromkeys(xdict[element]))
            xdict[element] = sorted(deduplicated)
        return xdict
    else:
        return xdict


def save_dict2json(path, xdict):
    with open(path, 'w') as fp:
        json.dump(xdict, fp, sort_keys=True, indent=4)


def order_and_save_dicts_to_json(dict_of_dicts):
    for x in dict_of_dicts:
        dict_of_dicts[x] = order_dict(dict_of_dicts[x])
        save_dict2json(path + x + '.json', dict_of_dicts[x])


path = "H:\\real_directory_here"

dict_of_dicts = load_jsons(path)
order_and_save_dicts_to_json(dict_of_dicts)
print('ALL GOOD')
