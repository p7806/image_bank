### Poster bot

A python script for scheduling posts on different platforms. I had it running for half a year and made hundreds of automated posts with it on insta before instabot got deprecated.
I'm sharing it now because some of the functionalities are might be valuable for someone.


### Functionalities of the scripts

poster_bot: Scheduler runs the implemented poster scripts
img_db: Keeps track of images
insta_poster: posts on insta [deprecated]
tagbank: chooses tags and mentions for post caption
post_logger: logs all posting
json_checker: standalone script to check validity of my json files containing hashtags


### Setup

You need `python>3.8` to run this script.

Install these libraries with your favourite method:
pandas
Pillow


### How to run?

A starting point would be to implement a posting script for each platform. Follow insta_poster.py as an example and add them in poster_bot.py


### Contribution / Citing the project

Hit me up at coding@tfb.anonaddy.me.