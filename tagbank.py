import random
import json
import os
import errno


class TagBank():
    def __init__(self, base_path, platform, imgspec_hashtags, img_categories, hts_limit=30):
        self.base_path = base_path
        self.hts_limit = hts_limit
        self.data_path = 'config\\tags\\'
        self.imgspec_hts = self.string_to_list(imgspec_hashtags)
        self.img_categories = img_categories
        # these hashtags are always to be included
        self.default_tags = self.load_data('default_tags.json')
        # region specific hashtags
        self.location_tags = self.load_data('location_tags.json')
        # hashtags that are not strongly connected to categories; any of them can be used on any kind of post
        self.basic_tags_dict = self.load_data('basic_tags.json')
        # collection of hashtags related to categories
        self.category_tags_dict = self.load_data('category_tags.json')
        # mentions added to the caption
        self.mentions_dict = self.load_data('mentions.json')

        if platform == 'instagram':
            self.chosen_hts = self.prepare_hts_for_insta()
            self.chosen_mentions = self.prepare_mentions_for_insta()
        else:
            print('TagBank only operates with insta till this moment')

    def string_to_list(self, strl):
        if strl == '':
            return []
        else:
            new_list = []
            for s in strl.split(','):
                new_list.append(s.strip())
                return new_list

    def get_chosen_hts(self):
        return self.chosen_hts

    def get_chosen_mentions(self):
        return self.chosen_mentions

    def choose_mention_tags(self, limit_default, limit_category):
        # there's no limit on total mentions
        mentions_l = []

        for category in self.mentions_dict:
            if category == 'default':
                if limit_default == -1:
                    mentions_l = mentions_l + self.mentions_dict[category]
                else:
                    mentions_l = mentions_l + \
                        self.choose_randomly_from_list(
                            limit_default, self.mentions_dict[category])
            else:
                if category in self.img_categories:
                    if limit_category == -1:
                        mentions_l = mentions_l + self.mentions_dict[category]
                    else:
                        mentions_l = mentions_l + \
                            self.choose_randomly_from_list(
                                limit_category, self.mentions_dict[category])

        return mentions_l

    def prepare_mentions_for_insta(self):
        mentions_l = self.choose_mention_tags(
            limit_default=1, limit_category=1)
        mentions_l = self.deduplicate_list(mentions_l)
        mentions_l = self.add_ht_prefixes(mentions_l, '@')
        return mentions_l

    def choose_randomly_from_list(self, choose_n, ht_list):
        if choose_n <= len(ht_list) and choose_n >= 0:
            return random.sample(ht_list, choose_n)
        else:
            print('error here: choose_n should be between 0 and len(list) which is:', len(
                ht_list), 'while choose_n is:', choose_n)
            return ht_list

    def get_portion_of_list(self, portion, ht_list):
        ht_l_len = len(ht_list)

        if portion > 0 and portion <= 1:
            choose_n = int(portion*ht_l_len)
            return self.choose_randomly_from_list(choose_n, ht_list)

        elif portion > 1 and portion <= ht_l_len:
            return self.choose_randomly_from_list(portion, ht_list)

        elif portion > ht_l_len:
            return ht_list

        else:
            raise Exception("Portion is less than 0")

    def choose_img_cat_hts(self, limit_total, limit_per_category):
        all_img_spec_hts_set = set()
        for category in self.category_tags_dict:
            if category in self.img_categories:
                all_img_spec_hts_set = all_img_spec_hts_set | set(self.get_portion_of_list(
                    limit_per_category, self.category_tags_dict[category]))

        ht_set_len = len(all_img_spec_hts_set)

        if ht_set_len > limit_total:
            all_img_spec_hts_set = self.choose_randomly_from_list(
                limit_total, all_img_spec_hts_set)

        return list(all_img_spec_hts_set)

    def get_n_random_entry_from_dict(self, complete_dict, choose_n):
        random_entries = random.sample(list(complete_dict), choose_n)
        new_dict = {}
        for e in random_entries:
            new_dict[e] = complete_dict[e]
        return new_dict

    def choose_spat_hts(self, limit_total, limit_per_spat, max_num_spat=1):
        spat_d_len = len(self.location_tags)
        if max_num_spat > 0 and max_num_spat < spat_d_len:
            spat_dict = self.get_n_random_entry_from_dict(
                self.location_tags, max_num_spat)
        else:
            raise Exception("max_num_spat is out of range")

        spat_hts = set()
        for spat in spat_dict:
            subset = set(self.get_portion_of_list(
                limit_per_spat, spat_dict[spat]))
            spat_hts = spat_hts | subset

        spat_hts_len = len(spat_hts)
        if spat_hts_len > limit_total:
            spat_hts = self.choose_randomly_from_list(limit_total, spat_hts)

        return list(spat_hts)

    def create_set_of_all_hts_in_dict(self, ht_dict):
        ht_set = set()
        for cat in ht_dict:
            ht_set = ht_set | set(ht_dict[cat])
        return ht_set

    def choose_basic_tags(self, limit_total, limit_per_cat, ht_list, max_num_cat=-1):
        # todo: limit_per_cat == -1

        basic_d_len = len(self.basic_tags_dict)

        if max_num_cat == -1 and limit_per_cat == -1:
            ht_set = self.create_set_of_all_hts_in_dict(self.basic_tags_dict)
            ht_set = self.choose_randomly_from_list(limit_total, ht_set)
            return list(ht_set)

        if max_num_cat == -1 and limit_per_cat >= 1:
            ht_set = set()
            for cat in self.basic_tags_dict:
                subset_o_cat = set(self.get_portion_of_list(
                    limit_per_cat, self.basic_tags_dict[cat]))
                ht_set = ht_set | subset_o_cat
            ht_set = self.choose_randomly_from_list(limit_total, ht_set)
            return list(ht_set)

        elif max_num_cat > 0 and max_num_cat < basic_d_len:
            basic_dict = self.get_n_random_entry_from_dict(
                self.location_tags, max_num_cat)
        else:
            raise Exception(
                "max_num_cat in choosing basic hashtags is out of range")

    def add_ht_prefixes(self, ht_list, tag_symbol='#'):
        new_ht_list = []
        for ht in ht_list:
            htht = tag_symbol + ht
            new_ht_list.append(htht)
        return new_ht_list

    def deduplicate_list(self, ht_list):
        # it's an order preserving way of deduplicating a list
        return list(dict.fromkeys(ht_list).keys())

    def prepare_hts_for_insta(self):
        # todo: implement different modes
        image_category_related_hashtags = self.choose_img_cat_hts(
            limit_total=15, limit_per_category=0.33)
        image_region_specific_hashtags = self.choose_spat_hts(
            limit_total=5, limit_per_spat=2)

        hashtags = self.imgspec_hts +\
            self.default_tags +\
            image_category_related_hashtags +\
            image_region_specific_hashtags

        hashtags = self.deduplicate_list(hashtags)

        # fill up the remaining space with basic hashtags
        while len(hashtags) != self.hts_limit:
            hashtags_len = len(hashtags)
            basic_tags_limit = self.hts_limit - hashtags_len
            hashtags = hashtags + self.choose_basic_tags(
                limit_total=basic_tags_limit, limit_per_cat=2, ht_list=hashtags, max_num_cat=-1)
            hashtags = self.deduplicate_list(hashtags)

        hashtags = self.add_ht_prefixes(hashtags, tag_symbol='#')
        return hashtags

    def save_dict(self, save_this_dict, fname):
        with safe_open_w(self.base_path + self.data_path + fname) as fp:
            json.dump(save_this_dict, fp, sort_keys=True, indent=4)

    def load_data(self, fname):
        with open(self.base_path + self.data_path + fname, 'r') as fp:
            return json.load(fp)


def mkdir_p(path):
    # Taken from https://stackoverflow.com/a/600612/119527
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def safe_open_w(path):
    ''' Open "path" for writing, creating any parent directories as needed.
    https://stackoverflow.com/questions/23793987/write-file-to-a-directory-that-doesnt-exist
    '''
    mkdir_p(os.path.dirname(path))
    return open(path, 'w')

# tiers of hashtags
    # 0 default tags
    # 1 location related tags
    # 2 image specific tags - htags added manually to be included in the list- strictly regarding the posted image
    # 3 series specific tags - at this point it's non-existent
    # 4 img category tags - at the moment assigned manually or tied to series
    # 5 basic hashtags divided into categories: they can be all used for any image
