import os
import pandas as pd
from datetime import datetime
import re
import random
import json
import re


class img_db():
    def __init__(self, base_path, img_db_file='img_db.csv'):

        self.extensions = ('png', 'jpg')
        self.base_path = base_path
        self.config_path = 'config\\imgs\\'
        self.img_path = base_path + 'img\\'
        self.img_db_file_path = os.path.join(
            base_path, self.config_path, img_db_file)
        self.series_dict = self.load_data('img_series.json')
        self.type_dict = self.load_data('img_type.json')
        self.img_df = None
        self.prepare_img_df()

    def load_data(self, fname):
        with open(self.base_path + self.config_path + fname, 'r') as fp:
            return json.load(fp)

    def list_img_without_series(self):
        filter_noseries = self.img_df['serie'] == 'NOSERIE'
        noseries_df = self.img_df[filter_noseries]
        if len(noseries_df.index) > 0:
            print('Attention! These images have no series yet\n\n', noserie_df)

    def prepare_img_df(self):
        if self.img_df_exists():

            self.update_img_df()
            self.save_img_df()
        else:
            img_list = self.discover_img()
            self.img_df = self.create_img_df(img_list)
            self.save_img_df()

    def update_img_df(self):
        img_df = self.open_img_df()
        # create a new img_df that has new files
        img_list = self.discover_img()
        img_df_new = self.create_img_df(img_list)
        # compare new and old img_df, add new lines
        img_df_updated = self.add_imgs_to_imgdb(img_df, img_df_new)
        # check if each picture in the db is existing in the folder
        # can happen that i delete a file and it's still in the db afterwards
        img_df_updated = self.update_img_dirs(img_df_updated, img_df_new)

        img_df_updated = self.update_img_series(img_df_updated)

        img_df_updated = self.update_img_type(img_df_updated)
        # update tobeposted column
        img_df_updated = self.update_tobeposted(img_df_updated)

        self.img_df = img_df_updated

    def save_img_df(self):
        file_name = 'img_db.csv'
        path = self.base_path + self.config_path + file_name
        try:
            self.img_df.to_csv(path, sep=';', index=False)
        except Exception as e:
            print('unable to save dataframe; error:\n', e)

    def img_df_exists(self):
        return os.path.exists(self.img_db_file_path)

    def discover_img(self):
        img_list = []

        for root, dirs, files in os.walk(self.img_path):
            for filename in files:
                if filename.endswith(self.extensions):
                    reltative_dir = root.replace(self.img_path, '')
                    img_list.append([self.img_path, reltative_dir, filename])
        return img_list

    def create_img_df(self, img_list):
        img_df = pd.DataFrame(columns=['img_fname',
                                       'path',
                                       'serie',
                                       'title',
                                       'cyear',
                                       'type',
                                       'caption',
                                       'fhashtags',
                                       'gradient',
                                       'img_added_date',
                                       'platforms',
                                       'to_be_posted',
                                       'priority',
                                       'post_count',
                                       'last_posted_date'
                                       ])

        for path_imgf in img_list:
            base_dir = path_imgf[0]
            relative_dir = path_imgf[1] + '\\'
            img = path_imgf[2]

            img_serie = self.set_serie(relative_dir + img)

            creation_year = self.set_creation_year_of_img(
                base_dir + relative_dir + img)

            img_title = self.set_img_title(img)
            img_type = self.set_img_type(img)
            forced_hashtags = ''
            caption = ''
            gradient = self.set_gradient(img)
            platforms = ''
            img_added_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            to_be_posted = self.set_tobeposted(img_serie, img_type)
            priority = self.set_priority()
            post_count = self.set_post_count()
            last_posted_date = self.set_last_posted_date()

            img_df = img_df.append({'img_fname': img,
                                    'path': relative_dir,
                                    'serie': img_serie,
                                    'title': img_title,
                                    'cyear': creation_year,
                                    'type': img_type,
                                    'caption': caption,
                                    'fhashtags': forced_hashtags,
                                    'gradient': gradient,
                                    'img_added_date': img_added_date,
                                    'platforms': platforms,
                                    'to_be_posted': to_be_posted,
                                    'priority': priority,
                                    'post_count': post_count,
                                    'last_posted_date': last_posted_date},
                                   ignore_index=True)
        return img_df

    def set_img_title(self, img):
        return 'Untitled'

    def set_serie(self, img_file_name):
        for s in self.series_dict:
            if s in img_file_name:
                return self.series_dict[s]

        series_from_folder = img_file_name.split('\\')[-2]

        if match:= re.search('^[a-z]{2,3} [0-9]{2,3} *- *([A-Za-z 0-9-]*)', series_from_folder):
            return match.group(1)

        if match:= re.search('^([A-Za-z ]+)$', series_from_folder):
            return match.group(1)
        else:
            # not part of a series or just forgot to have an serie entry in the dict
            return 'NOSERIE'

    def set_img_type(self, img_file_name):
        for s in self.type_dict:
            if s in img_file_name:
                return self.type_dict[s]
        return 'NOTYPE'

    def set_tobeposted(self, img_serie, img_type):
        if img_serie == 'NOSERIE' or img_type == 'NOTYPE':
            return False
        else:
            return True

    def set_post_count(self):
        # to be implemented: how many times was it posted
        return 0

    def set_last_posted_date(self):
        # timestamp for last posting time
        return ''

    def set_priority(self):
        # to be implemented: priority is between 1-5 where 1 is highest
        return 3

    def choose_img(self, mode='random'):
        filter_post = self.img_df['to_be_posted'] == True
        img_to_be_posted_df = self.img_df[filter_post]

        if mode == 'random-unposted':
            filter_post_count = img_to_be_posted_df['post_count'] == 0
            img_to_be_posted_df = img_to_be_posted_df[filter_post_count]
            chosen_index = random.randint(0, len(img_to_be_posted_df.index)-1)

        if mode == 'random':
            chosen_index = random.randint(0, len(img_to_be_posted_df.index)-1)
        if mode == 'newest':
            pass
        if mode == 'oldest':
            pass
        if mode == 'priority':
            pass

        random_row_from_df = img_to_be_posted_df.iloc[chosen_index]

        return random_row_from_df

    def open_img_df(self):
        return pd.read_csv(self.img_db_file_path, sep=';', na_filter=False)

    def update_img_series(self, img_db):
        for row_num in range(len(img_db.index)):
            row = img_db.iloc[row_num]
            series = self.set_serie(row['path'] + row['img_fname'])
            img_db.loc[img_db['img_fname'] ==
                       row['img_fname'], 'serie'] = series
        return img_db

    def update_img_type(self, img_db):
        cnt = 0
        for row_num in range(len(img_db.index)):
            row = img_db.iloc[row_num]
            img_type = row['type']

            new_type = self.set_img_type(row['path'] + row['img_fname'])
            if new_type != img_type:
                cnt += 1
                img_db.loc[img_db['img_fname'] ==
                           row['img_fname'], 'type'] = new_type

        if cnt > 0:
            print(cnt, 'image types were updated')
        return img_db

    def update_posted_img_row(self, img_fname):
        self.img_df.loc[self.img_df['img_fname'] == img_fname, 'post_count'] = int(
            self.img_df.loc[self.img_df['img_fname'] == img_fname, 'post_count']) + 1
        self.img_df.loc[self.img_df['img_fname'] == img_fname,
                        'last_posted_date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.save_img_df()

    def delete_row(self, img_title):
        pass

    def get_relative_path(self, dir):
        return dir.replace(self.base_path, '')

    def update_img_dirs(self, img_df, img_df_new):
        '''
            check if the old entries are having a correct directory
                yes     >   unchanged
                no      >   change
                nonex   >   delete entry? even if it was posted before?
        '''
        existing_img_df = img_df
        current_file_list = img_df_new['img_fname'].values
        delete_entries = []
        change_cnt = 0

        for row_num in range(len(existing_img_df.index)):
            row = existing_img_df.iloc[row_num]
            if row['img_fname'] not in current_file_list:
                # delete row from img_db
                delete_entries.append(row['img_fname'])
            else:
                # it is there and they have the same dirs: do nothing
                condition = img_df_new['img_fname'] == row['img_fname']
                if img_df_new[condition]['path'].iloc[0] != row['path']:

                    where_img_fname_equals_in_existing = existing_img_df['img_fname'] == row['img_fname']
                    where_img_fname_equals_in_current = img_df_new['img_fname'] == row['img_fname']

                    existing_img_df.loc[where_img_fname_equals_in_existing, 'path'] = self.get_relative_path(
                        img_df_new[where_img_fname_equals_in_current]['path'].iloc[0])
                    change_cnt += 1

        if change_cnt > 0:
            print(change_cnt, 'directories were updated')

        if len(delete_entries) > 0:
            print(len(delete_entries), 'rows were deleted:', delete_entries)

        existing_img_df = existing_img_df[~existing_img_df['img_fname'].isin(
            delete_entries)]

        return existing_img_df

    def add_imgs_to_imgdb(self, img_df, img_df_alt):

        merged_img_df = img_df
        file_list = merged_img_df['img_fname'].values

        cnt = 0
        for row_num in range(len(img_df_alt.index)):
            row = img_df_alt.iloc[row_num]

            if row['img_fname'] not in file_list:
                merged_img_df = merged_img_df.append(row)
                cnt += 1
        if cnt > 0:
            print(cnt, 'new images were added to the img_db')

        return merged_img_df

    def update_tobeposted(self, img_df):

        filter_noserie = img_df['serie'] == 'NOSERIE'
        filter_notype = img_df['type'] == 'NOTYPE'
        img_df['to_be_posted'] = ~ (filter_noserie | filter_notype)
        return img_df

    def set_creation_year_of_img(self, img_path):

        # if the filename contains a year we use that otherwise the modification year
        match = re.search(r'(2[0-9]{3})', img_path)
        if match is not None:
            year = match.group(1)
            current_year = datetime.now().year
            year_dt = datetime.strptime(year, '%Y').year
            if year_dt <= current_year:
                return year_dt
            else:
                print('error: we have a future creation year at this pic', img_path)
                return '2022'
        else:
            mod_date = os.path.getmtime(img_path)
            mod_dt = datetime.fromtimestamp(mod_date)
            return mod_dt.year

    def set_gradient(self, img_file_name):
        return ''
