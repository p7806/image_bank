import os
import pandas as pd
from PIL import Image
import time
from datetime import datetime
from datetime import timedelta
import random
import string
import re
import insta_poster
import schedule
import tagbank
from img_db import img_db
import post_logger as pl


class post_scheduler():
    def __init__(self, p_freq, base_path, mock_run=False):
        self.base_path = base_path
        self.img_db = img_db(self.base_path)
        self.p_freq = p_freq
        self.posting_times = 'empty'
        self.platform_dict = {'instagram': 'post_on_insta'}
        self.status_dict = dict()
        self.mock_run = mock_run
        self.logger = pl.post_logger(self.base_path)

    def generate_intervals(self, start_time_str=None, end_time_str=None):

        now = datetime.now()

        if start_time_str is None:
            start_time = now
        else:
            st = datetime.strptime(start_time_str, '%H:%M')
            start_time = now.replace(hour=st.hour, minute=st.minute)

        if end_time_str is None:
            end_time = now.replace(hour=23, minute=59)

        else:
            et = datetime.strptime(end_time_str, '%H:%M')
            end_time = now.replace(hour=et.hour, minute=et.minute)

        seconds_difference = (end_time - start_time).total_seconds()

        seconds_in_one_interval = seconds_difference/self.p_freq

        l = (pd.DataFrame(columns=['NULL'],
                          index=pd.date_range(start_time, end_time,
                                              freq=str(seconds_in_one_interval)+'S')).index.strftime('%Y-%m-%d %H:%M:%S').tolist())
        return l

    def str_time_prop(self, start, end, time_format, prop):
        # from https://stackoverflow.com/questions/553303/generate-a-random-date-between-two-other-dates
        """Get a time at a proportion of a range of two formatted times.

        start and end should be strings specifying times formatted in the
        given format (strftime-style), giving an interval [start, end].
        prop specifies how a proportion of the interval to be taken after
        start.  The returned time will be in the specified format.
        """

        stime = time.mktime(time.strptime(start, time_format))
        etime = time.mktime(time.strptime(end, time_format))

        ptime = stime + prop * (etime - stime)

        return time.strftime(time_format, time.localtime(ptime))

    def generate_random_times_in_intervals(self, start_time_str=None, end_time_str=None):
        intervals = self.generate_intervals(start_time_str, end_time_str)
        time_format = '%Y-%m-%d %H:%M:%S'
        random_times = []
        for n in range(len(intervals)-1):
            random_times.append(self.str_time_prop(
                intervals[n], intervals[n+1], time_format, random.random()))
        return random_times

    def scheduler_daily(self, start_time=None, end_time=None):
        schedule.every().day.at("00:00").do(self.schedule_today, start_time, end_time)
        while True:
            schedule.run_pending()
            time.sleep(1)

    def schedule_now(self):
        now = datetime.now()
        post_time = now + timedelta(seconds=10)
        t = post_time.strftime('%H:%M:%S')
        schedule.every().day.at(t).do(self.post_image)

    def schedule_today(self, start_time_str=None, end_time_str=None, mode='all'):

        if mode == 'all':

            self.posting_times = self.generate_random_times_in_intervals(
                start_time_str, end_time_str)

        if mode == 'partial':
            start_time_str = '00:00'
            end_time_str = '23:59'
            self.posting_times = self.generate_random_times_in_intervals(
                start_time_str, end_time_str)

            now = datetime.now()
            time_format = '%Y-%m-%d %H:%M:%S'
            new_posting_times = []
            for pt in self.posting_times:

                pt_dt = datetime.strptime(pt, time_format)

                if pt_dt > now:
                    new_posting_times.append(pt)

            self.posting_times = new_posting_times

        if mode == 'now_and_later':
            self.posting_times = self.generate_random_times_in_intervals(
                start_time_str, end_time_str)
            print(self.posting_times)
            now = datetime.now()
            post_time = now + timedelta(seconds=10)
            time_format = '%Y-%m-%d %H:%M:%S'
            t = post_time.strftime(time_format)
            self.posting_times[0] = t

        print(self.posting_times)
        for post_time in self.posting_times:
            print('post is scheduled at', post_time)
            post_time = datetime.strptime(post_time, '%Y-%m-%d %H:%M:%S')
            t = post_time.strftime('%H:%M:%S')
            schedule.every().day.at(t).do(self.post_image)

    def add_watermark(self, img):
        # to be implemented
        return img

    def preproc(self, img_row):

        max_height = 1080

        temp_folder = self.img_db.base_path + 'temp\\'

        if not os.path.exists(temp_folder):
            os.makedirs(temp_folder)

        temp_img_name = temp_folder + \
            self.random_string(random.randint(7, 11)) + '.jpg'

        img_name = self.img_db.img_path + \
            img_row['path'] + img_row['img_fname']

        with Image.open(img_name) as img:
            img = self.resize_img(img, max_height)
            rgb_img = img.convert('RGB')
            rgb_img = self.add_watermark(rgb_img)
            rgb_img.save(temp_img_name)

        return temp_img_name

    def resize_img(self, img, max_height):
        # this metod now enlarging smaller images, maybe it's overkill
        img_height = float(img.size[1])
        ratio = max_height / img_height
        new_width = int((float(img.size[0]) * ratio))
        return img.resize((new_width, max_height), Image.ANTIALIAS)

    def random_string(self, length):
        chars = string.digits + string.ascii_letters
        return ''.join(random.choice(chars) for i in range(length))

    def post_on_platforms(self, img_row, temp_img_name):
        ''' If platforms are empty then post it in each implemented platform
            otherwise post it on each platform which is mentioned in this entry img_row['platforms']
        '''
        if img_row['platforms'] == '':
            for platform in self.platform_dict:
                method_name = self.platform_dict[platform]

                try:
                    posting_method = getattr(self, method_name)
                except AttributeError:
                    raise NotImplementedError("Class `{}` does not implement `{}`".format(
                        self.__class__.__name__, method_name))

                self.status_dict[platform] = posting_method(
                    img_row, temp_img_name)

                if not self.mock_run:
                    self.log_post(platform, img_row, temp_img_name)

        else:
            for platform in self.platform_dict:
                if platform in img_row['platforms']:
                    # if platform was specified in the img data then we get that posting method from the platform_dict
                    method_name = self.platform_dict[platform]
                    try:
                        # creating a function out of the 'posting method string' and  specifing that it is a method of self
                        posting_method = getattr(self, method_name)
                    except AttributeError:
                        raise NotImplementedError("Class `{}` does not implement `{}`".format(
                            self.__class__.__name__, method_name))

                    # running the posting func
                    self.status_dict[platform] = posting_method(
                        img_row, temp_img_name)
                    if not self.mock_run:
                        self.log_post(platform, img_row, temp_img_name)

    def post_on_insta(self, img_row, temp_img_name):
        print('Post on insta method is running here')
        IP = insta_poster.insta_poster(
            self.base_path, img_row, temp_img_name, mock_run=self.mock_run)
        # fix: it seems that the class is not destroyed after the return
        status = IP.get_status()
        del IP
        return status

    def post_image(self):

        # 0 update img_db - maybe delete those rows here which contain nonexistent files
        self.img_db.update_img_df()

        # 1 choose picture from database
        img_row = self.img_db.choose_img(mode='random-unposted')
        print(img_row['img_fname'])

        # 2 preproc
        temp_img_name = self.preproc(img_row)

        # 3 post
        self.post_on_platforms(img_row, temp_img_name)

        # 4 postproc
        self.postproc(temp_img_name, img_row['img_fname'])

        print('posting times:', self.posting_times)

        # 5 log
        # self.log_post(paltform,img_row,temp_img_name)

        return schedule.CancelJob

    def postproc(self, temp_img_fname, img_fname):

        successful_posting = False
        for platform in self.status_dict:
            s = self.status_dict[platform]
            status_exists = s is not None and s is not False
            if status_exists:
                successful_posting = True

        if successful_posting:
            self.update_img_db(img_fname)
            self.remove_temp_file(temp_img_fname)

    def update_img_db(self, img_fname):
        self.img_db.update_posted_img_row(img_fname)

    def remove_temp_file(self, temp_img_fname):
        try:
            os.remove(temp_img_fname + '.REMOVE_ME')
        except:
            print('could not delete temp file')


    def log_post(self, platform, img_row, temp_file_name):
        status_dict = self.status_dict[platform]
        now = datetime.now()
        time_format = '%Y-%m-%d %H:%M:%S'
        post_ts = now.strftime(time_format)

        full_caption = status_dict['caption']['text']
        tags = ''
        caption_message = ''
        if match:= re.search('(#.*)$', full_caption):
            tags = match.group(1)
        series = img_row['serie']
        mode = ''
        url = 'https://www.instagram.com/p/' + status_dict['code'] + '/'
        temp_file_name = temp_file_name.split('\\')[-1]

        self.logger.log_post(
            post_ts, img_row['img_fname'], temp_file_name, series, caption_message, tags, mode, platform, url)


def main():
    base_path = "H:\\F\\bot_heimat\\torok.exp\\"
    posting_freq = 2
    mock = True
    ps = post_scheduler(posting_freq, base_path, mock_run=mock)
    start_time = '11:00'
    end_time = '23:00'
    # mode='all','now_and_later','partial'
    ps.schedule_today(mode='now_and_later')
    # fix: partial does the partial job in the period of the whole day and not the given time frame!
    ps.scheduler_daily(start_time, end_time)


if __name__ == "__main__":
    main()
