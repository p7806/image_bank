import tagbank
from instabot import Bot
import time
import os
import json


class insta_poster():
    def __init__(self, base_path, img_row, temp_img_name, mock_run=False):

        self.status = False
        self.success = False
        self.base_path = base_path
        self.img_row = img_row
        self.tin = temp_img_name
        self.tagbank_c = tagbank.TagBank(self.base_path, platform='instagram',
                                         imgspec_hashtags=self.img_row['fhashtags'], img_categories=self.img_row['type'])
        self.hashtags = self.tagbank_c.get_chosen_hts()
        self.mentions = self.tagbank_c.get_chosen_mentions()
        self.credentials_dict = self.load_data('config\\credentials.json')
        self.user = self.credentials_dict['instagram_user']
        self.password = self.credentials_dict['instagram_pass']
        self.caption = self.prepare_caption()

        if mock_run is False:
            self.clear_cookie_cash()
            self.bot = Bot()
            self.log_in()
            self.post()
            self.log_out()
            self.clear_cookie_cash()
            self.get_status()
            del self.bot
        else:
            print('Successfully MOCK posted')
            self.success = True

    def load_data(self, fname):
        with open(self.base_path + fname, 'r') as fp:
            return json.load(fp)

    def log_in(self):
        self.bot.login(username=self.user, password=self.password)
        time.sleep(0.5)
        pass

    def clear_cookie_cash(self):
        try:
            os.remove('config//' + self.user + '_uuid_and_cookie.json')
        except:
            print('could not delete config file')

    def prepare_caption(self):
        caption = ''
        # to implement: get next personal message from stack
        personal_message = ""
        img_specific_text = self.img_row['caption']
        title = ' \n \n' + self.img_row['serie'] + ' - ' + \
            self.img_row['title'] + ' - ' + str(self.img_row['cyear'])

        if personal_message != '':
            personal_message = personal_message + '\n\n'

        if img_specific_text != '':
            img_specific_text = img_specific_text + '\n\n'

        line_brake = '\n ' * 12

        hashtags = ' '.join(self.hashtags)
        hashtags = hashtags.strip()

        mentions = ' ' + ' '.join(self.mentions)

        caption = personal_message + img_specific_text + \
            title + line_brake + hashtags + mentions

        print(caption)
        return caption

    def post(self):
        self.status = self.bot.upload_photo(self.tin, caption=self.caption)
        self.success = self.status is not None and self.status is not False
        time.sleep(0.5)

    def log_out(self):
        self.bot.logout()

    def get_status(self):
        return self.status
