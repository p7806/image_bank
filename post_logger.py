import os
import csv


class post_logger():

    def __init__(self, base_dir):
        self.base_dir = base_dir
        self.log_rel_dir = 'config\\logger\\'
        self.log_fname = 'post_log.csv'

    def log_post(self, timestamp, filename, temp_filename, series, caption, tags, mode, platform, url):
        log_dir = self.base_dir + self.log_rel_dir
        file_dir = log_dir + self.log_fname

        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        columns = ['timestamp', 'filename', 'temp_filename',
                   'series', 'caption', 'tags', 'mode', 'platform', 'url']
        row_dict = {'timestamp': timestamp,
                    'filename': filename,
                    'temp_filename': temp_filename,
                    'series': series,
                    'caption': caption,
                    'tags': tags,
                    'mode': mode,
                    'platform': platform,
                    'url': url}

        try:
            with open(file_dir, 'a', newline='') as csvfile:
                writer = csv.DictWriter(
                    csvfile, fieldnames=columns, delimiter=';')
                writer.writerow(row_dict)

        except IOError:
            print("I/O error")
        except BaseException as err:
            print(f"Unexpected {err=}, {type(err)=}")
